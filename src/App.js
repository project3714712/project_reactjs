import React, {Component, useState} from 'react';
import {withRouter, useLocation} from 'react-router-dom';
import axios from "axios";
import Head from './Component/Layout/Head';
import Footer from './Component/Layout/Footer';
import MenuLeft from './Component/Layout/MenuLeft';
import MenuAcc from './Component/Layout/MenuAcc';
import Slider from './Component/Layout/Slider';
import None from './Component/Layout/None';
import {UserContext} from './Component/Context/UserContext';

function App(props) {
    let params1 = useLocation();
    const [countQTY, setQTY] = useState(0);
    function getQTY(data){
        console.log(data)
        setQTY(data)
    }
  return (
    <>
        <UserContext.Provider value={{
            getQTY: getQTY,
            countQTY:countQTY
            }}>
            <Head  />
            <section>
                <div className='container'>
                    <div className='row'>
                        {/* {params2['pathname'].includes("cart") ? <None /> : <None />} */}
                        {params1['pathname'].includes("account") ? <MenuAcc /> : <MenuLeft />} 
                        {props.children}
                    </div>
                </div>
            </section>
            <Footer />
        </UserContext.Provider>
    </>
  );
}

export default App;
