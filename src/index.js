import React from 'react';
import ReactDOM from 'react-dom/client';
import {
    BrowserRouter as Router,
    Routes,
    Route
} from 'react-router-dom';

import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Blog from './Component/Blog/Blog';
import Detail from './Component/Blog/Detail';
import Reg from './Component/Account/Reg';
import Login from './Component/Account/Login';
import Profile from './Component/Account/Profile';
import Listpr from './Component/Product/Listpr';
import Addpr from './Component/Product/Addpr';
import Editpr from './Component/Product/Editpr';
import Home from './Component/Home/Home';
import Detailpr from './Component/Product/Detailpr';
import Cart from './Component/Cart/Cart';
import Wishlist from './Component/Wishlist/Wishlist';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
    <React.StrictMode>
        <Router>
            <App>
                <Routes>
                    <Route index path='/home' element={<Home />}  />
                    <Route path='/blog/list' element={<Blog />} />
                    <Route path='/blog/detail/:id' element={<Detail />} />
                    <Route path='/reg' element={<Reg />}/>
                    <Route path='/login' element={<Login />}/>
                    <Route path='/account/update' element={<Profile />}/>
                    <Route path='/account/product/list' element={<Listpr />}/>
                    <Route path='/account/product/add' element={<Addpr />}/>
                    <Route path='/account/product/edit/:id' element={<Editpr />}/>
                    <Route path='/product/detail/:id' element={<Detailpr />}/>
                    <Route path='/cart' element={<Cart />}/>
                    <Route path='/wishlist' element={<Wishlist />}/>
                </Routes>
            </App>
        </Router>
     </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
