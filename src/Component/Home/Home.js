import React, {Component, useEffect, useState, useContext} from 'react';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link
} from 'react-router-dom';
import axios from "axios";
import { UserContext } from '../Context/UserContext';
function Home(props) {
    const [data, setData] = useState([]);
    const data2 =  localStorage.getItem("appState");
    const userData = JSON.parse(data2);
        useEffect(() => {
            axios.get("http://localhost:8087/laravel8/public/api/product")
            .then(res=>{
                setData(res.data.data)
                // console.log(res.data.data)
            })
            .catch(error => console.log(error))    
        },[])
   
    function fetchData(){
        if(data.length > 0){
            return data.map((value, key)=>{
                let img = JSON.parse(value['image']) 
                return (
                    <div className="col-sm-4">
                    <div className="product-image-wrapper">
                      <div className="single-products">
                        <div className="productinfo text-center">
                          <img src={"http://localhost:8087/laravel8/public/upload/product/"+ value.id_user+ "/" + img[0]} alt="" />
                          <h2>{value.price}</h2>
                          <p>{value.name}</p>
                          <a name={value.id} onClick={getidPr} className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                        </div>
                        <div className="product-overlay">
                          <div className="overlay-content">
                          <h2>{value.price}</h2>
                          <p>{value.name}</p>
                            <a name={value.id} onClick={getidPr} className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                          </div>
                        </div>
                      </div>
                      <div className="choose">
                        <ul className="nav nav-pills nav-justified">
                          <li><a name={value.id} onClick={getidList}><i className="fa fa-plus-square" />Add to wishlist</a></li>
                          <li><Link to={"/product/detail/" + value.id}><i className="fa fa-plus-square" />More</Link></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                    )
            })
        }
    }

    const value = useContext(UserContext)

    const getidList = (e) => {
        e.preventDefault();
        let getId = e.target.name

        let checklist =  localStorage.getItem("wish_list");
        checklist = JSON.parse(checklist)

        if(checklist){
            console.log(checklist)
            // if(checklist.includes(getId)){
            //     console.log("Có rồi")
            //     localStorage.setItem("wish_list",JSON.stringify(checklist));
            // }
            if(checklist.includes(getId)){
                localStorage.setItem("wish_list",JSON.stringify(checklist));
            }else{
                checklist.push(getId)
                localStorage.setItem("wish_list",JSON.stringify(checklist));
            }
            }else{
                let list_pr = []
                list_pr.push(getId)
                localStorage.setItem("wish_list",JSON.stringify(list_pr));
            }
    };

    const getidPr = (e) => {
        e.preventDefault();
        let getId = e.target.name
        let checkcart =  localStorage.getItem("cart_list");
        checkcart = JSON.parse(checkcart)
        if(checkcart){
            if(checkcart[getId]){
                checkcart[getId]++
                let QTY = 0
                Object.keys(checkcart).map((value, key)=>{
                    QTY = QTY + checkcart[value]
                })
                value.getQTY(QTY)
                localStorage.setItem("cart_list",JSON.stringify(checkcart))

            }else{
                        let cart_pr = { }
                        cart_pr[getId] = 1;
                        checkcart = { ...checkcart, ...cart_pr };
                        let QTY = 0
                        Object.keys(checkcart).map((value, key)=>{
                            QTY = QTY + checkcart[value]
                        })
                        value.getQTY(QTY)
                        localStorage.setItem("cart_list",JSON.stringify(checkcart));
                    }
        }else{
            let cart_pr = { }
            cart_pr[getId] = 1;
            localStorage.setItem("cart_list",JSON.stringify(cart_pr));
            value.getQTY(1)
        }
    };
    

return (
        <div className="col-sm-9 padding-right">
          <div className="features_items">
            <h2 className="title text-center">Features Items</h2>
                    {fetchData()}
          </div>

        </div>      
    )
  }
  export default Home;
  