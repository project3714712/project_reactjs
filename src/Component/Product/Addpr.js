import React, {Component, useEffect, useState} from 'react';
import axios from "axios";
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    useNavigate
} from 'react-router-dom';
import FormError from '../Error/FormError';

function Addpr(props) {
    const [isOpen, setIsOpen] = useState(false);
    const [data, setData] = useState([]);
    const [inputs, setInputs] = useState({
        Name:"",
        price:"",
        category:""
    })
    const [errors, setErrors] = useState({});
    const [getFile, setFile] = useState("");
    const[getAvatar, setAvatar] = useState("");

    useEffect(() => {
        axios.get("http://localhost:8087/laravel8/public/api/category-brand")
        .then(res=>{
            setData(res.data)
            // console.log(res.data)
        })
        .catch(error => console.log(error))    
    },[])

    function fetchDataB(){
        if(Object.keys(data).length > 0){
            return data.brand.map((value,key)=>{
                return (
                    <option value={value.id}>{value.brand}</option>
                );
            })
        }
    }

    function fetchDataC(){
        if(Object.keys(data).length > 0){
            return data.category.map((value,key)=>{
                return (
                    <option value={value.id}>{value.category}</option>
                );
            })
        }
    }

    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}))
    }

    const ChooseValue = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}))
        if(nameInput == "Sale" && value == 1 ){
            setIsOpen(true);
        }else{
            setIsOpen(false);
        }
    }
    
    function hanldeFile(e){
        const file = e.target.files; 
        setFile(file)
    }

    function handleSubmit(e){
        e.preventDefault();
        let errorsSubmit={};
        let flag = true;

        if(inputs.Name ==""){
            errorsSubmit.Name = "Vui lòng nhập tên";
            flag = false;
        }

        if(inputs.Price == ""){
            errorsSubmit.Price = "Vui lòng nhập giá tiền";
            flag = false;
        }

        if(inputs.Category==""){
            errorsSubmit.Category = "Vui lòng chọn Category";
            flag = false;
        }

        if(inputs.Brand ==""){
            errorsSubmit.Brand = "Vui lòng chọn Brand";
            flag = false;
        }

        if(inputs.Company ==""){
            errorsSubmit.Company = "Vui lòng nhập Company";
            flag = false;
        }

        if(inputs.message ==""){
            errorsSubmit.message = "Vui lòng nhập detail";
            flag = false;
        }

        if(inputs.Sale ==""){
            errorsSubmit.Sale = "Vui lòng chọn Status";
            flag = false;
        }

        if(getFile == ""){
            errorsSubmit.img = "Vui lòng chọn hình ảnh";
            flag = false;
        }
        else if(getFile.length > 3){
            errorsSubmit.img = "Vui lòng chọn không quá 3 ảnh";
            flag = false;
        }
        else {
            let arr = ['png', 'jpg', 'jpeg', 'PNG', 'JPG'];
            let sumsize = 0
            for(let file of getFile){
                let size = file['size'];
                sumsize = sumsize + size
                let name = file['name'];
                let name2 = name.split(".");
                if(sumsize > 1024 * 1024){
                    errorsSubmit.img = "Vui lòng chọn avatar có dung lượng thấp";
                    // console.log("Vui lòng chọn avatar có dung lượng thấp")
                    flag = false;
                }
                else if(arr.includes(name2[1]) == false){
                    errorsSubmit.img = "Vui lòng chọn đúng file";
                    // console.log("Vui lòng chọn đúng file")
                    flag = false;
                }
            }
        }

        if(!flag){
            setErrors(errorsSubmit);
        }
        else{
            const data2 =  localStorage.getItem("appState");
            const userData = JSON.parse(data2);

            let url = 'http://localhost:8087/laravel8/public/api/user/product/add';
            let accessToken = userData.token;

            let config = { 
                headers: { 
                'Authorization': 'Bearer '+ accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
                } 
            };
            const formData = new FormData();
            formData.append('name', inputs.Name);
            formData.append('price', inputs.Price);
            // formData.append('id_user', userData.Auth.id);
            formData.append('category', inputs.Category);
            formData.append('brand', inputs.Brand);
            formData.append('company', inputs.Company);
            formData.append('detail', inputs.message);
            formData.append('status', inputs.Sale);
            formData.append('sale', inputs.Sale2 ? inputs.Sale2 : 0);
            for(let file of getFile){
                formData.append('file[]', file);
            }

            axios.post("http://localhost:8087/laravel8/public/api/user/product/add", formData, config)
            .then(res =>{
                console.log(res)
                console.log('Thành công')
            })
            .catch(error => console.log(error))                
        }
    }

    return (		
        <div className="col-sm-8">
            <div className="signup-form" >
            <h2>Create Product !</h2>
            <FormError  errors={errors}/>
            <form onSubmit={handleSubmit} encType="multipart/form-data">

            <input type="text" name="Name" placeholder="Name" onChange={handleInput}/>
            <input type="text" name="Price" placeholder="Price"onChange={handleInput} />
            <select className="form-control form-control-line" name="Category" placeholder="Select Category" onChange={handleInput}>
                <option>Select Category</option> 
                {fetchDataC()}    
            </select>

            <select className="form-control form-control-line" name="Brand" placeholder="Select Brand" onChange={handleInput}>  
                <option>Select Brand</option>          
                {fetchDataB()}
            </select>

            <select className="form-control form-control-line" id="selector" name="Sale" onChange={ChooseValue} >            
                <option>Status</option>
                <option value={0}>New</option>
                <option value={1}>Sale</option >
            </select>

            {isOpen && <input className="colors"  type="text" name="Sale2" id="sale2"  placeholder="0%" onChange={handleInput}/> }
            <input type="text" name="Company"  placeholder="Company Profile" onChange={handleInput}/>
            <input multiple type="file" className="form-control form-control-line" name="file[]"  onChange={hanldeFile}/>
            <textarea cols={30} rows={7} name="message" placeholder="Detail Product"  onChange={handleInput}/>
            <button type="submit" className="btn btn-default">OK</button>
            </form>
            </div>
        </div>
    );
}
 
  export default Addpr;