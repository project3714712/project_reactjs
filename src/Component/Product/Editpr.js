import React, {Component, useEffect, useState} from 'react';
import axios from "axios";
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    useNavigate,
    useParams
} from 'react-router-dom';
import FormError from '../Error/FormError';

function Editpr(props) {
    let params = useParams();
    const [isOpen, setIsOpen] = useState(false);
    const [errors, setErrors] = useState({});
    const [checkimg, setCheckimg] = useState([]);
    const [getFile, setFile] = useState("");
    const [data, setData] = useState([]);
    const [dataB, setDataB] = useState([]);
    const [inputs, setInputs] = useState({
        Name:"",
        Price:"",
        Category:"",
        Brand:"",
        Company:"",
        message:"",
        status:"",
        Sale2:"",
    })

    const data2 =  localStorage.getItem("appState");
    const userData = JSON.parse(data2);

    let accessToken = userData.token;
        
        let config = { 
            headers: { 
            'Authorization': 'Bearer '+ accessToken,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
            } 
        };

    useEffect(() => {
        axios.get("http://localhost:8087/laravel8/public/api/user/product/" + params.id, config)
        .then(res=>{
            setData(res.data.data)
            console.log(res.data.data)
            inputs.Name = res.data.data.name
            inputs.Price = res.data.data.price
            inputs.Category = res.data.data.id_category
            inputs.Brand = res.data.data.id_brand
            inputs.Company = res.data.data.company_profile
            inputs.message = res.data.data.detail
            inputs.status = res.data.data.status
            if(res.data.data.status == 1){
                inputs.Sale2 = res.data.data.sale
                setIsOpen(true);
            }
        })
        .catch(error => console.log(error))    
    },[])

    useEffect(() => {
        axios.get("http://localhost:8087/laravel8/public/api/category-brand")
        .then(res=>{
            setDataB(res.data)
            // console.log(res.data)
        })
        .catch(error => console.log(error))    
    },[])

    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}))
        if(nameInput == "Sale" && value == 1 ){
            setIsOpen(true);
        }
        if(nameInput == "New" && value == 0){
            setIsOpen(false);
        }
    }

    const handlecheck = (e) => {
        const nameInput = e.target.name;
        const checked = e.target.checked;
        if(nameInput && checked){
            setCheckimg([...checkimg, nameInput])
        }else{
            if(checkimg.includes(nameInput) && checked == false){
                setCheckimg(checkimg.filter(item => item !== nameInput))
            }
        } 
    }

    function fetchDataB(){
        if(Object.keys(dataB).length > 0){
            return dataB.brand.map((value,key)=>{
                if(data.id_brand == value.id){
                    return (
                        <>
                            <option selected="selected" value={value.id}>{value.brand}</option>
                            {
                                dataB.brand.map((value2,key2)=>{
                                    if(data.id_brand != value2.id){
                                        return(
                                            <option value={value2.id}>{value2.brand}</option>
                                        )
                                    }
                                })
                            }
                        </>
                    );
                }  
            })
        }
    }

    function fetchDataC(){
        if(Object.keys(dataB).length > 0){
            return dataB.category.map((value,key)=>{
                if(data.id_category == value.id){
                    return (
                        <>
                            <option selected="selected" value={value.id}>{value.category}</option>
                            {
                                dataB.category.map((value2,key2)=>{
                                    if(data.id_category != value2.id){
                                        return(
                                            <option value={value2.id}>{value2.category}</option>
                                        )
                                    }
                                })
                            }
                        </>
                    );
                }  
            })
        }
    }

    function fetchDataS(){
        if(Object.keys(data).length > 0){
            if(data.status == 0){
                return (
                    <>
                        <option selected value={0}>New</option>
                        <option value={1}>Sale</option>
                    </>
                )
            }

            if(data.status == 1){
                return (
                    <>
                        <option value={0}>New</option>
                        <option selected value={1}>Sale</option>
                    </>
                )
            }
        }
    }

    function fetchDataI(){
        if(Object.keys(data).length >0 ){
            if(data.image.length > 0){
                return data.image.map((value,key)=>{
                        return(
                            <div style={{display: "inline-block",width: "100%",marginTop:"5px"}}>
                                    <img style={{width: "200px",height:"200px"}} id={value} htmlFor="checkimg" src={"http://localhost:8087/laravel8/public/upload/product/"+ userData.Auth.id + "/" + value} /> 
                                    <input style={{display: "inline-block", width: "50%"}} type="checkbox" for={value} name={value} onChange={handlecheck} />
                            </div>
                        )
                })
            }
        }
    }

    function hanldeFile(e){
        const file = e.target.files; 
        setFile(file)
    }

    function handleSubmit(e){
        e.preventDefault();
        let errorsSubmit={};
        let flag = true;

        if(inputs.Name ==""){
            errorsSubmit.Name = "Vui lòng nhập tên";
            flag = false;
        }

        if(inputs.Price == ""){
            errorsSubmit.Price = "Vui lòng nhập giá tiền";
            flag = false;
        }

        if(inputs.Category==""){
            errorsSubmit.Category = "Vui lòng chọn Category";
            flag = false;
        }

        if(inputs.Brand ==""){
            errorsSubmit.Brand = "Vui lòng chọn Brand";
            flag = false;
        }

        if(inputs.Company ==""){
            errorsSubmit.Company = "Vui lòng nhập Company";
            flag = false;
        }

        if(inputs.message ==""){
            errorsSubmit.message = "Vui lòng nhập detail";
            flag = false;
        }

        // console.log(inputs.status)
        // if(inputs.status == ""){
        //     errorsSubmit.Sale = "Vui lòng chọn Status";
        //     flag = false;
        // }

        if(getFile == ""){
            errorsSubmit.img = "Vui lòng chọn hình ảnh";
            flag = false;
        }
        else if(getFile.length > 3){
            errorsSubmit.img = "Vui lòng chọn không quá 3 ảnh";
            flag = false;
        }
        else if(getFile.length + data.image.length - checkimg.length > 3){
            errorsSubmit.img = "Vui lòng giảm bớt ảnh chọn hoặc xóa bớt ảnh";
            flag = false;
        }
        else {
            let arr = ['png', 'jpg', 'jpeg', 'PNG', 'JPG'];
            let sumsize = 0;
            for(let file of getFile){
                let size = file['size'];
                sumsize = sumsize + size
                let name = file['name'];
                let name2 = name.split(".");
                if(sumsize > 1024 * 1024){
                    errorsSubmit.img = name + " Có dung lượng lớn, vui lòng chọn avatar có dung lượng thấp";
                    console.log(name + " Có dung lượng lớn, vui lòng chọn avatar có dung lượng thấp");
                    flag = false;
                }
                else if(arr.includes(name2[1]) == false){
                    errorsSubmit.img = name + " Không phải là file ảnh";
                    console.log(name + " Không phải là file ảnh");
                    flag = false;
                }
            }
        }

        if(!flag){
            setErrors(errorsSubmit);
        }
        else{
            const formData = new FormData();
            formData.append('name', inputs.Name);
            formData.append('price', inputs.Price);
            formData.append('category', inputs.Category);
            formData.append('brand', inputs.Brand);
            formData.append('company', inputs.Company);
            formData.append('detail', inputs.message);
            formData.append('status', inputs.status);
            formData.append('sale', inputs.Sale2 ? inputs.Sale2 : 0);
            for(let file of getFile){
                formData.append('file[]', file);
            }
            for(let i = 0; i < checkimg.length ; i++){
                formData.append('avatarCheckBox[]', checkimg[i]);
            }
            
            axios.post("http://localhost:8087/laravel8/public/api/user/product/update/" + params.id, formData, config)
            .then(res =>{
                console.log(res)
                console.log('Thành công')
            })
            .catch(error => console.log(error))       

        }
    }
        
    function fetchData(){
        if(Object.keys(data).length >0 ){
            return (
                <div className="signup-form" >
                    <h2>Edit Product !</h2>
                    <FormError  errors={errors}/>
                    <form onSubmit={handleSubmit} encType="multipart/form-data">
                        <input type="text" name="Name" defaultValue={data.name} placeholder="Name" onChange={handleInput}/>
                        <input type="text" name="Price" defaultValue={data.price} placeholder="Price" onChange={handleInput}/>

                        <select className="form-control form-control-line" name="Category" placeholder="Select Category" onChange={handleInput}>
                            <option>Select Category</option>
                            {fetchDataC()}
                        </select>

                        <select className="form-control form-control-line" name="Brand" placeholder="Select Brand" onChange={handleInput}>
                            <option>Select Brand</option>      
                            {fetchDataB()}
                        </select>

                        <select className="form-control form-control-line" name="status" onChange={handleInput}>            
                            <option>Status</option>
                            {fetchDataS()}
                         </select>

                        {isOpen && <input className="colors"  type="text" name="Sale2" id="sale2" defaultValue={data.sale} placeholder="0%" onChange={handleInput}/> }
                        <input type="text" name="Company" defaultValue={data.company_profile} placeholder="Company Profile" onChange={handleInput}/>
                        <input multiple type="file" className="form-control form-control-line" name="file[]" onChange={hanldeFile}/>

                        {fetchDataI()}

                        <textarea cols={30} rows={7} name="message" placeholder="Detail Product" defaultValue={data.detail} onChange={handleInput}/>

                        <button type="submit" className="btn btn-default">OK</button>
                    </form>
                </div>
            )
        }
    }

    
    return (        
  <div className="col-sm-8">
    {fetchData()}
  </div>
    );
}
  
  export default Editpr;