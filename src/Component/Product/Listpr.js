import React, {Component, useEffect, useState} from 'react';
import axios from "axios";
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    useNavigate
} from 'react-router-dom';
import FormError from '../Error/FormError';

function Listpr(props) {
    const [data, setData] = useState([]);
    const data2 =  localStorage.getItem("appState");
    const userData = JSON.parse(data2);

    let accessToken = userData.token;
        
        let config = { 
            headers: { 
            'Authorization': 'Bearer '+ accessToken,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
            } 
        };

        useEffect(() => {
            axios.get("http://localhost:8087/laravel8/public/api/user/my-product", config)
            .then(res=>{
                setData(res.data.data)
                console.log(res.data.data)
            })
            .catch(error => console.log(error))    
        },[])

    function fetchData(){
        if( Object.keys(data).length > 0){
            return Object.keys(data).map((value,key)=>{
                let img = JSON.parse(data[value]['image']) 
                return (
                    <tr>
                        <td>{data[value]['id']}</td>
                        <td>{data[value]['name']}</td>
                        <td><img style={{width: "200px",height:"200px"}} src={"http://localhost:8087/laravel8/public/upload/product/"+ data[value]['id_user']+ "/" + img[0]} /> </td>
                        <td>{data[value]['price']}</td>
                        <td><Link to={"/account/product/edit/" + data[value]['id'] }>Edit</Link></td>
                        <td><Link id={data[value]['id']} onClick = {getidPr} >Delete</Link></td>
                    </tr>
                );
            })
        }   
    }

    const getidPr = (e) => {
        e.preventDefault();
        let ID = e.target.id
            axios.get("http://localhost:8087/laravel8/public/api/user/product/delete/" + ID, config)
            .then(res=>{
                setData(res.data.data)
                // console.log(res.data.data)
            })
            .catch(error => console.log(error))    
    };

    return (		
        <div className="col-sm-8">
        <section id="cart_items">
          <div className="table-responsive cart_info">    
            <table className="table table-condensed">
              <thead>
                <tr className="cart_menu">
                  <td className="image">ID</td>
                  <td className="description">Name</td>
                  <td className="quantity">Image</td>
                  <td className="price">Price</td>
                  <td className="total">Action</td>
                  <td />
                </tr>
              </thead>
              <tbody>
                    {fetchData()}
                </tbody>
            </table>
          </div>
          <Link to="/account/product/add" className="btn btn-default">ADD</Link>
        </section> 
      </div>
      
    );

  }
  
  export default Listpr;