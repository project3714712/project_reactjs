import React, {Component, useEffect, useState} from 'react';
import axios from "axios";
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    useNavigate,
    useParams
} from 'react-router-dom';
import FormError from '../Error/FormError';
import Image from 'react-bootstrap/Image'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Modal from 'react-bootstrap/Modal'
import ModalHeader from 'react-bootstrap/ModalHeader'
import ModalTitle from 'react-bootstrap/ModalTitle'
import ModalBody from 'react-bootstrap/ModalBody'
import ModalFooter from 'react-bootstrap/ModalFooter'
import Button from 'react-bootstrap/Button'
function Detailpr(props) {
    let params = useParams();
    const [data, setData] = useState([]);
    const [src, setSrc ] = useState();
    const data2 =  localStorage.getItem("appState");
    const userData = JSON.parse(data2);
    useEffect(() => {
        axios.get("http://localhost:8087/laravel8/public/api/product/detail/" + params.id)
        .then(res=>{
            setData(res.data.data)
            let img = JSON.parse(res.data.data['image'])
            setSrc("http://localhost:8087/laravel8/public/upload/product/"+ res.data.data.id_user + "/" + img[1])
            // console.log(res.data.data)
        })
        .catch(error => console.log(error))    
    },[])

    const getsrc = (e) => {
        e.preventDefault();
        let getsrc = e.target.src
        setSrc(getsrc)
    };

    // const getsrc123 = (e) => {
    //     e.preventDefault();
    //     console.log(123)
        
    // };

      

    function fetchData(){
        if(Object.keys(data).length >0){
            let img = JSON.parse(data['image']) 
            return(
                <>
                <div className="col-sm-5">
                        <div className="view-product">
                                
                            <img src={src} alt="" />
                            <a href={src} ><h3>ZOOM</h3></a>
                                    
                        </div>
                        <div id="similar-product" className="carousel slide" data-ride="carousel">
                        
                        <div className="carousel-inner">
                            <div className="item active">
                            <Row md={12}>
                                <a href>
                                <Col xs={6} md={4}>
                                    <Image src={"http://localhost:8087/laravel8/public/upload/product/"+ data.id_user + "/" + img[0]} thumbnail onClick = {getsrc}/>
                                </Col>
                                </a>

                                <a href>
                                <Col xs={6} md={4}>
                                    <Image src={"http://localhost:8087/laravel8/public/upload/product/"+ data.id_user + "/" + img[1]} thumbnail onClick = {getsrc}/>
                                </Col>
                                </a>

                                <a href>
                                <Col xs={6} md={4}>
                                    <Image src={"http://localhost:8087/laravel8/public/upload/product/"+ data.id_user + "/" + img[2]} thumbnail onClick = {getsrc}/>
                                </Col>
                                </a>
                            </Row>
                            </div>
                            {/* <div className="item">
                            <a href><img style={{width: "90px",height:"90px"}} src={"http://localhost:8087/laravel8/public/upload/product/"+ userData.Auth.id + "/" + img[0]} alt="" /></a>
                            <a href><img style={{width: "90px",height:"90px"}} src={"http://localhost:8087/laravel8/public/upload/product/"+ userData.Auth.id + "/" + img[1]} alt="" /></a>
                            <a href><img style={{width: "90px",height:"90px"}} src={"http://localhost:8087/laravel8/public/upload/product/"+ userData.Auth.id + "/" + img[2]} alt="" /></a>
                            </div> */}
                        </div>

                        <a className="left item-control" href="#similar-product" data-slide="prev">
                            <i className="fa fa-angle-left" />
                        </a>
                        <a className="right item-control" href="#similar-product" data-slide="next">
                            <i className="fa fa-angle-right" />
                        </a>
                        </div>
                    </div>
                    
                    <div className="col-sm-7">
                        <div className="product-information">
                        <img src="images/product-details/new.jpg" className="newarrival" alt="" />
                        <h2>{data.name}</h2>
                        <p>Web ID: {data.id}</p>
                        <img src="images/product-details/rating.png" alt="" />
                        <span>
                            <span>{data.price}</span>
                            <label>Quantity:</label>
                            <input type="text" defaultValue={1} />
                            <button type="button" className="btn btn-fefault cart">
                            <i className="fa fa-shopping-cart" />
                            Add to cart
                            </button>
                        </span>
                        <p><b>Availability:</b> In Stock</p>
                        <p><b>Condition:</b> New</p>
                        <p><b>Brand:</b> E-SHOPPER</p>
                        <a href><img src="images/product-details/share.png" className="share img-responsive" alt="" /></a>
                        </div>
                    </div>
            </>
            )
        } 
    }

    return (		
            <div className="col-sm-9 padding-right">
                <div className="product-details">
                    {fetchData()}
                    
                </div>

                {/* <div className="category-tab shop-details-tab">
                <div className="col-sm-12">
                    <ul className="nav nav-tabs">
                    <li><a href="#details" data-toggle="tab">Details</a></li>
                    <li><a href="#companyprofile" data-toggle="tab">Company Profile</a></li>
                    <li><a href="#tag" data-toggle="tab">Tag</a></li>
                    <li className="active"><a href="#reviews" data-toggle="tab">Reviews (5)</a></li>
                    </ul>
                </div>

                <div className="tab-content">
                    <div className="tab-pane fade" id="details">
                    <div className="col-sm-3">
                        <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                            <img src="images/home/gallery1.jpg" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                            <img src="images/home/gallery2.jpg" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                            <img src="images/home/gallery3.jpg" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                            <img src="images/home/gallery4.jpg" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="tab-pane fade" id="companyprofile">
                    <div className="col-sm-3">
                        <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                            <img src="images/home/gallery1.jpg" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                            <img src="images/home/gallery3.jpg" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                            <img src="images/home/gallery2.jpg" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                            <img src="images/home/gallery4.jpg" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="tab-pane fade" id="tag">
                    <div className="col-sm-3">
                        <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                            <img src="images/home/gallery1.jpg" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                            <img src="images/home/gallery2.jpg" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                            <img src="images/home/gallery3.jpg" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                            <img src="images/home/gallery4.jpg" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="tab-pane fade active in" id="reviews">
                    <div className="col-sm-12">
                        <ul>
                        <li><a href><i className="fa fa-user" />EUGEN</a></li>
                        <li><a href><i className="fa fa-clock-o" />12:41 PM</a></li>
                        <li><a href><i className="fa fa-calendar-o" />31 DEC 2014</a></li>
                        </ul>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <p><b>Write Your Review</b></p>
                        <form action="#">
                        <span>
                            <input type="text" placeholder="Your Name" />
                            <input type="email" placeholder="Email Address" />
                        </span>
                        <textarea name defaultValue={""} />
                        <b>Rating: </b> <img src="images/product-details/rating.png" alt="" />
                        <button type="button" className="btn btn-default pull-right">
                            Submit
                        </button>
                        </form>
                    </div>
                    </div>
                </div>
                
                </div> */}

                {/* <div className="recommended_items">
                <h2 className="title text-center">recommended items</h2>
                <div id="recommended-item-carousel" className="carousel slide" data-ride="carousel">
                    <div className="carousel-inner">
                    <div className="item active">	
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/recommend1.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/recommend2.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/recommend3.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="item">	
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/recommend1.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/recommend2.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/recommend3.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <a className="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                    <i className="fa fa-angle-left" />
                    </a>
                    <a className="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                    <i className="fa fa-angle-right" />
                    </a>			
                </div>
                </div> */}

            </div>

    );
}
 
  export default Detailpr;