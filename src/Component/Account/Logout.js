import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    useNavigate
} from 'react-router-dom';

const Logout = function() {
    localStorage.removeItem('checklog');
    const navigate  = useNavigate();
    let check = localStorage.getItem("checklog");
    if(!check){
        navigate('/login');
    }
    }
  
export default Logout;
