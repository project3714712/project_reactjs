import React, {Component, useEffect, useState} from 'react';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link
} from 'react-router-dom';

import axios from "axios";
import FormError from '../Error/FormError';
import api from '../API/api';

function Reg(props) {
    const [inputs, setInputs] = useState({
        name:"", 
        email:"",
        password:"",
        phone:"",
        address:"",
    })

    const [errors, setErrors] = useState({});
    const [getFile, setFile] = useState("");
    const[getAvatar, setAvatar] = useState("");

    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}))
    }

    function IsEmail(email) {
        let regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
          return false;
        }else{
          return true;
        }
    }

    function hanldeFile(e){
        const file = e.target.files;

        let reader = new FileReader();
        reader.onload = (e) => {
            setAvatar(e.target.result);
            setFile(file[0]);
        };
        reader.readAsDataURL(file[0]);
    }
     

    function handleSubmit(e){
        e.preventDefault();
        let errorsSubmit={};
        let flag = true;

        if(inputs.email ==''){
            errorsSubmit.email = "Vui lòng nhập Email";
            flag = false;
        }else if(IsEmail(inputs.email)==false){
            errorsSubmit.notanemail = "Không đúng định dạng email";
            flag = false;
        }

        if(inputs.name ==""){
            errorsSubmit.name = "Vui lòng nhập số điện thoại";
            flag = false;
        }

        if(inputs.password == ""){
            errorsSubmit.password = "Vui lòng nhập pass";
            flag = false;
        }

        if(inputs.phone ==""){
            errorsSubmit.phone = "Vui lòng nhập số điện thoại";
            flag = false;
        }

        if(inputs.address ==""){
            errorsSubmit.address = "Vui lòng nhập địa chỉ";
            flag = false;
        }


        if(getFile == ""){
            errorsSubmit.avatar = "Vui lòng chọn avatar";
            flag = false;
        }else{
            let arr = ['png', 'jpg', 'jpeg', 'PNG', 'JPG'];
            console.log(getFile['name']) 
            let size = getFile['size'];
            let name = getFile['name'];
            let name2 = name.split(".");
            if(size > 1024 * 1024){
                errorsSubmit.avatar1 = "Vui lòng chọn avatar có dung lượng thấp";
                flag = false;
            }
            else if(arr.includes(name2[1]) == false){
                errorsSubmit.avatar2 = "Vui lòng chọn đúng file";
                flag = false;
            }
        }

        if(!flag){
            setErrors(errorsSubmit);
        }else{
            const data = {
                email: inputs.email,
                name: inputs.name,
                password: inputs.password,
                phone: inputs.phone,
                address: inputs.address,
                avatar: getAvatar,
                level: 0
            } 

            axios.post("http://localhost:8087/laravel8/public/api/register" , data)
            .then(res=>{
                if(res.data.errors){
                    setErrors(res.data.errors);
                }else{
                    console.log(res.data)
                    console.log('Thành công')

                }
            })
            .catch(error => console.log(error))    
        
        }
        
    }
        
    
    return ( 
        <div className="col-sm-4">
        <div className="signup-form">

        <FormError  errors={errors}/>

          <h2>New User Signup!</h2>
          <form onSubmit={handleSubmit} encType="multipart/form-data">
            <input type="text" name="name" placeholder="Name" onChange={handleInput} />
            <input type="email" name="email" placeholder="Email" onChange={handleInput}/>
            <input type="password" name="password" placeholder="Password" onChange={handleInput}/>
            <input type="text" name="phone" placeholder="Phone" onChange={handleInput}/>
            <input type="text" name="address" placeholder="Address" onChange={handleInput}/>
            <input type='file' name='avatar' onChange={hanldeFile} />
            <button type="submit" className="btn btn-default">Signup</button>
          </form>
        </div>
      </div>
    );
  }
  
  export default Reg;
  