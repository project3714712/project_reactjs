import React, {Component, useEffect, useState } from 'react';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    useNavigate
} from 'react-router-dom';

import axios from "axios";
import FormError from '../Error/FormError';
import api from '../API/api';

function Login(props) {
    const [inputs, setInputs] = useState({
        email:"",
        password:""
    })
    const [errors, setErrors] = useState({});

    const navigate = useNavigate();
    const [data, setData] = useState([]);

    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}))
    }

    function IsEmail(email) {
        let regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
          return false;
        }else{
          return true;
        }
    }


    function handleSubmit(e){
        e.preventDefault();
        let errorsSubmit={};
        let flag = true;

        if(inputs.email ==''){
            errorsSubmit.email = "Vui lòng nhập Email";
            flag = false;
        }else if(IsEmail(inputs.email)==false){
            errorsSubmit.notanemail = "Không đúng định dạng email";
            flag = false;
        }
        // else if(inputs.email !=  0) {
        //     errorsSubmit.email = "Không đúng email";
        //     flag = false;
        // }

        if(inputs.password == ""){
            errorsSubmit.pass = "Vui lòng nhập pass";
            flag = false;
        }
        // else if(inputs.password !=0 ){
        //     errorsSubmit.pass = "Không đúng mật khẩu"
        //     flag = false;
        // }
        
        if(!flag){
            setErrors(errorsSubmit);
        }else{
            const data = {
                email: inputs.email,
                password: inputs.password,
                level: 0
            } 

            axios.post("http://localhost:8087/laravel8/public/api/login" , data)
            .then(res=>{
                if(res.data.errors){
                    setErrors(res.data.errors);
                }else{
                    console.log(res.data)
                    console.log('Thành công')
                    localStorage.setItem("appState",JSON.stringify(res.data));
                    const Checklog = true;
                    localStorage.setItem("checklog",JSON.stringify(Checklog));
                    navigate('/');
                }
            })
            .catch(error => console.log(error))    
        
           
        }

    }
        
    
    return ( 
        <div className="col-sm-4 col-sm-offset-1">
        <div className="login-form">
          <h2>Login to your account</h2>
          <FormError  errors={errors}/>
          <form onSubmit={handleSubmit}>
            <input type="email" name="email" placeholder="Email Address" onChange={handleInput} />
            <input type="password" name="password"  placeholder="Password" onChange={handleInput} />
            <span>
              <input type="checkbox" className="checkbox" /> 
              Keep me signed in
            </span>
            <button type="submit" className="btn btn-default">Login</button>
          </form>
        </div>
      </div>
    );
  }
  
  export default Login;
  