import React, {Component, useEffect, useState } from 'react';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    useNavigate
} from 'react-router-dom';

import axios from "axios";
import FormError from '../Error/FormError';
import api from '../API/api';

function Profile(props) {
    const [errors, setErrors] = useState({});
    const [user, setUser] = useState({
        username: "",
        email: "",
        address: "",
        phone: "",
        password:""
    });
    const [getFile, setFile] = useState("");
    const[getAvatar, setAvatar] = useState("");
    let Avatar = ""
    const data2 =  localStorage.getItem("appState");
    const userData = JSON.parse(data2);

    // console.log(userData);

    useEffect(() => {
            if(userData) {
                setUser({
                    username: userData.Auth.name,
                    email: userData.Auth.email,
                    address: userData.Auth.address,
                    phone: userData.Auth.phone
                })
            }
    },[])

    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setUser(state => ({...state,[nameInput]:value}))
    }

    function hanldeFile(e){
        const file = e.target.files;

        let reader = new FileReader();
        reader.onload = (e) => {
            setAvatar(e.target.result);
            setFile(file[0]);
        };
        reader.readAsDataURL(file[0]);
    }

    function handleSubmit(e){
        e.preventDefault();
        let errorsSubmit={};
        let flag = true;

        if(user.username ==''){
            errorsSubmit.name = "Không để trống tên";
            flag = false;
        }

        if(user.address ==''){
            errorsSubmit.name = "Không để trống địa chỉ";
            flag = false;
        }

        // if(user.country ==''){
        //     errorsSubmit.name = "Không để trống id_country";
        //     flag = false;
        // }

        if(user.phone ==''){
            errorsSubmit.phone = "Không để trống số điện thoại";
            flag = false;
        }

        if(getFile != ""){
            let arr = ['png', 'jpg', 'jpeg', 'PNG', 'JPG'];
            // console.log(getFile['name']) 
            let size = getFile['size'];
            let name = getFile['name'];
            let name2 = name.split(".");
            if(size > 1024 * 1024){
                errorsSubmit.avatar1 = "Vui lòng chọn avatar có dung lượng thấp";
                flag = false;
            }
            else if(arr.includes(name2[1]) == false){
                errorsSubmit.avatar2 = "Vui lòng chọn đúng file";
                flag = false;
            }
            else{
                 Avatar = getFile['name'];
                 console.log(Avatar)
            }
        }

        if(!flag){
            setErrors(errorsSubmit);
        }else{
            let url = 'http://localhost:8087/laravel8/public/api/user/update/' + userData.Auth.id;
                let accessToken = userData.token;
    
                let config = { 
                    headers: { 
                    'Authorization': 'Bearer '+ accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                    } 
                };
                
                
                
                const formData = new FormData();
                formData.append('name', user.username);
                formData.append('email', userData.Auth.email);
                formData.append('phone', user.phone);
                formData.append('address', user.address);
                formData.append('country', 2);
                formData.append('password', user.password ? user.password : "");
                formData.append('avatar', Avatar ? Avatar : "");

                axios.post( url, formData, config)
                .then(res =>{
                    console.log(res)
                    console.log('Thành công')
                })
                .catch(error => console.log(error))  
        }
    }
    function fetchData(){
        return (
            <div className="signup-form">
                <h2>User Update !</h2>
                <FormError  errors={errors}/>
                <form onSubmit={handleSubmit} encType="multipart/form-data">
                <input type="text" name="username"  placeholder="username" defaultValue={userData.Auth.name} onChange={handleInput}/>
                <input type="email" name="email" placeholder="email" defaultValue={userData.Auth.email} readonly/>
                <input type="password" name="password" placeholder="password"  onChange={handleInput}/>
                <input type="text" name="address" placeholder="address" defaultValue={userData.Auth.address} onChange={handleInput}/>
                <input type="text" name="country" placeholder="country"  defaultValue={userData.Auth.country} onChange={handleInput}/>
                <input type="text" name="phone" placeholder="phone"  defaultValue={userData.Auth.phone} onChange={handleInput}/>
                <input type="file" name="avatar"  onChange={hanldeFile}/>
                <button type="submit" className="btn btn-default">OK</button>
                </form>
            </div>
        )
    }
    
    return ( 
        <div className="col-sm-4">
            {fetchData()}
        </div>
    );
  }
  
  export default Profile;