import React, {useState} from 'react';

function FormError(props){
    // let {errors} = props;
    // console.log(props.errors);
    function renderError() {
        if(Object.keys(props.errors).length > 0){
            return Object.keys(props.errors).map((key, index) => {
                return (
                    <li key={index}>{props.errors[key]}</li>
                )
            })
        }
    }
    return(
        <ul>
            {renderError()}
        </ul>
    )
}

export default FormError;