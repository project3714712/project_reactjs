import React, {Component, useEffect, useState, useContext} from 'react';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link
} from 'react-router-dom';
import axios from "axios";
import { UserContext } from '../Context/UserContext';

function Wishlist(props) {
    let checklist =  localStorage.getItem("wish_list");
    checklist = JSON.parse(checklist)
    var new_data = [];
    const [data, setData] = useState([]);
        useEffect(() => {
            axios.get("http://localhost:8087/laravel8/public/api/product/wishlist")
            .then(res=>{
                setData(res.data.data)
                // console.log(res.data.data)
            })
            .catch(error => console.log(error))    
        },[])
    
        

        function fetchData(){
            if(new_data.length > 0){
                return new_data.map((value, key)=>{
                    let img = JSON.parse(value['image']) 
                    return (
                        <div className="col-sm-4">
                        <div className="product-image-wrapper">
                          <div className="single-products">
                            <div className="productinfo text-center">
                              <img src={"http://localhost:8087/laravel8/public/upload/product/"+ value.id_user+ "/" + img[0]} alt="" />
                              <h2>{value.price}</h2>
                              <p>{value.name}</p>
                              <a name={value.id} onClick={getidPr} className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                            </div>
                            <div className="product-overlay">
                              <div className="overlay-content">
                              <h2>{value.price}</h2>
                              <p>{value.name}</p>
                                <a name={value.id} onClick={getidPr} className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                              </div>
                            </div>
                          </div>
                          <div className="choose">
                            <ul className="nav nav-pills nav-justified">
                              <li><a name={value.id} onClick={getidList}><i className="fa fa-plus-square" />Remove this product</a></li>
                              <li><Link to={"/product/detail/" + value.id}><i className="fa fa-plus-square" />More</Link></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                        )
                })
            }
        }

        const value = useContext(UserContext)
        
        for (var i = 0; i < data.length; i++){
            for (var j = 0; j < checklist.length; j++){
                if(data[i]['id'] == checklist[j]){
                    new_data.push(data[i]);
                }
            }
        }
        
        const getidList = (e) => {
            e.preventDefault();
            let getId = e.target.name
            if(checklist){
                for (let i = 0; i < checklist.length; i++) {
                    if(checklist[i] == getId){
                        const new_arr = checklist.splice(i,1)
                        new_data = checklist
                        console.log(checklist)
                        localStorage.setItem("wish_list",JSON.stringify(checklist))
                    }                    
                }
            }
        };

        const getidPr = (e) => {
            e.preventDefault();
            let getId = e.target.name
            let checkcart =  localStorage.getItem("cart_list");
            checkcart = JSON.parse(checkcart)
            if(checkcart){
                if(checkcart[getId]){
                    checkcart[getId]++
                    let QTY = 0
                    Object.keys(checkcart).map((value, key)=>{
                        QTY = QTY + checkcart[value]
                    })
                    value.getQTY(QTY)
                    localStorage.setItem("cart_list",JSON.stringify(checkcart))
    
                }else{
                            let cart_pr = { }
                            cart_pr[getId] = 1;
                            checkcart = { ...checkcart, ...cart_pr };
                            let QTY = 0
                            Object.keys(checkcart).map((value, key)=>{
                                QTY = QTY + checkcart[value]
                            })
                            value.getQTY(QTY)
                            localStorage.setItem("cart_list",JSON.stringify(checkcart));
                        }
            }else{
                let cart_pr = { }
                cart_pr[getId] = 1;
                localStorage.setItem("cart_list",JSON.stringify(cart_pr));
                value.getQTY(1)
            }
        };

return (
        <div className="col-sm-9 padding-right">
          <div className="features_items">
            <h2 className="title text-center">Features Items</h2>
                    {fetchData()}
          </div>
        </div>      
    )
  }
  export default Wishlist;
  