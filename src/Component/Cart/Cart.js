import React, {Component, useEffect, useState, useContext} from 'react';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link
} from 'react-router-dom';
import axios from "axios";
import { UserContext } from '../Context/UserContext';

function Cart(props) {
    const [data, setData] = useState([]);
    const [errors, setErrors] = useState({});
    const data2 =  localStorage.getItem("appState");
    const userData = JSON.parse(data2);
    let total_all = 0
    let checkcart =  localStorage.getItem("cart_list");
    checkcart = JSON.parse(checkcart)
    useEffect(() => {
    axios.post("http://localhost:8087/laravel8/public/api/product/cart",checkcart)
    .then(res =>{
        setData(res.data.data)
        // console.log(res.data.data)
    })
    .catch(error => console.log(error))
    },[])
    
    function fetchData(){
        if(data.length > 0){
            // console.log(data)
            for (let i = 0; i <data.length; i++){
                total_all = (total_all + data[i]["price"] * data[i]['qty'])
            }
            return data.map((value, key)=>{
                let img = JSON.parse(value['image'])
                let total = value.qty * value.price
                return (
                    <tr>
                        <td className="cart_image">
                            <a><img style={{width: "70%", height:"100px"}} src={"http://localhost:8087/laravel8/public/upload/product/"+ value.id_user+ "/" + img[0]} alt="" /></a>
                        </td>
                        <td className= 'cart_description'>
                            <h4><a>{value.name}</a></h4>
                            <p>Web ID: {value.id}</p>
                        </td>
                        <td className='cart_price'>
                            <p>{value.price} $</p>
                        </td>
                        <td className= 'cart_quantity'>
                            <div className= 'cart_quantity_button'>
                                    <a name={value.id} onClick={getidPr} className= 'cart_quantity_down' >-</a>
                                    <input class='cart_quantity_input' type='text' name='quantity' value={value.qty} autocomplete='off' size='2' />
                                    <a name={value.id} onClick={getidPr} className= 'cart_quantity_up' >+</a>
                            </div>
                        </td>
                        <td className='cart_total'>
                            <p className='cart_total_price'> {total} $</p>
                        </td>
                        <td className='cart_image'>
                                <a  name={value.id} onClick={getidPr} href='#' class='cart_quantity_delete' >X</a>
                        </td>
                    </tr>
                )
            })
        }
    }

    const value = useContext(UserContext)

    const getidPr = (e) => {
        e.preventDefault();
        let getId = e.target.name
        let getText = e.target.text
        let newData = [...data];
        if(getText == "+"){
            for (let i = 0; i <newData.length; i++){
                if(newData[i]['id'] == getId){
                    newData[i]['qty']++
                    setData(newData)
                    if(checkcart){
                        if(checkcart[getId]){
                            checkcart[getId]++
                            let QTY = 0
                            Object.keys(checkcart).map((value, key)=>{
                                QTY = QTY + checkcart[value]
                            })
                            value.getQTY(QTY)
                            localStorage.setItem("cart_list",JSON.stringify(checkcart));
                        }
                    }
                }
            }  
        }
        
        if(getText == "-"){
            for (let i = 0; i <newData.length; i++){
                if(newData[i]['id'] == getId){
                    newData[i]['qty']--
                    if(newData[i]['qty'] == 0){
                        newData.splice(i,1)
                    }
                    setData(newData)
                    if(checkcart){
                        if(checkcart[getId]){
                            checkcart[getId]--
                            let QTY = 0
                                Object.keys(checkcart).map((value, key)=>{
                                    QTY = QTY + checkcart[value]
                                })
                                value.getQTY(QTY) 
                            localStorage.setItem("cart_list",JSON.stringify(checkcart));
                            if(checkcart[getId] == 0){
                                delete checkcart[getId]
                                localStorage.setItem("cart_list",JSON.stringify(checkcart));
                                if(Object.keys(checkcart).length == 0){
                                    localStorage.removeItem("cart_list");
                                }
                            }
                        }
                    }
                }
            }  
        }

        if(getText == "X"){
            for (let i = 0; i <newData.length; i++){
                if(newData[i]['id'] == getId){
                    newData[i]['qty']=0
                        if(newData[i]['qty'] == 0){
                            newData.splice(i,1)
                        }
                        setData(newData)
                    if(checkcart){
                        if(checkcart[getId]){
                            checkcart[getId] = 0
                            let QTY = 0
                                Object.keys(checkcart).map((value, key)=>{
                                    QTY = QTY + checkcart[value]
                                })
                                value.getQTY(QTY) 
                            localStorage.setItem("cart_list",JSON.stringify(checkcart));
                            if(checkcart[getId] == 0){
                                delete checkcart[getId] 
                                localStorage.setItem("cart_list",JSON.stringify(checkcart));
                                if(Object.keys(checkcart).length == 0){
                                    localStorage.removeItem("cart_list");
                                }
                            }
                        }
                    }
                }
            }  
        }

    };
    
return (
        <>
        <div>
                        <section id="cart_items">
                            <div className="container">
                            <div className="breadcrumbs">
                                <ol className="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li className="active">Shopping Cart</li>
                                </ol>
                            </div>
                            <div className="table-responsive cart_info">
                                <table className="table table-condensed">
                                <thead>
                                    <tr className="cart_menu">
                                    <td className="image">Image</td>
                                    <td className="description">Name</td>
                                    <td className="price">Price</td>
                                    <td className="quantity">Quantity</td>
                                    <td className="total">Total</td>
                                    <td />
                                    </tr>
                                </thead>
                                <tbody>
                                    {fetchData()}
                                </tbody>
                                </table>
                            </div>
                            </div>
                        </section> {/*/#cart_items*/}
                        
                        <section id="do_action">
                            <div className="container">
                            <div className="heading">
                                <h3>What would you like to do next?</h3>
                                <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
                            </div>
                            <div className="row">
                                <div className="col-sm-6">
                                <div className="chose_area">
                                    <ul className="user_option">
                                    <li>
                                        <input type="checkbox" />
                                        <label>Use Coupon Code</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" />
                                        <label>Use Gift Voucher</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" />
                                        <label>Estimate Shipping &amp; Taxes</label>
                                    </li>
                                    </ul>
                                    <ul className="user_info">
                                    <li className="single_field">
                                        <label>Country:</label>
                                        <select>
                                        <option>United States</option>
                                        <option>Bangladesh</option>
                                        <option>UK</option>
                                        <option>India</option>
                                        <option>Pakistan</option>
                                        <option>Ucrane</option>
                                        <option>Canada</option>
                                        <option>Dubai</option>
                                        </select>
                                    </li>
                                    <li className="single_field">
                                        <label>Region / State:</label>
                                        <select>
                                        <option>Select</option>
                                        <option>Dhaka</option>
                                        <option>London</option>
                                        <option>Dillih</option>
                                        <option>Lahore</option>
                                        <option>Alaska</option>
                                        <option>Canada</option>
                                        <option>Dubai</option>
                                        </select>
                                    </li>
                                    <li className="single_field zip-field">
                                        <label>Zip Code:</label>
                                        <input type="text" />
                                    </li>
                                    </ul>
                                    <a className="btn btn-default update" href>Get Quotes</a>
                                    <a className="btn btn-default check_out" href>Continue</a>
                                </div>
                                </div>
                                <div className="col-sm-6">
                                <div className="total_area">
                                    <ul>
                                    <li>Cart Sub Total <span>$</span></li>
                                    <li className="tax">Eco Tax <span>$2</span></li>
                                    <li>Shipping Cost <span>Free</span></li>
                                    <li>Total <span>{total_all} $</span></li>
                                    </ul>
                                    <a className="btn btn-default update" href>Update</a>
                                    <a className="btn btn-default check_out" href>Check Out</a>
                                </div>
                                </div>
                            </div>
                            </div>
                        </section>{/*/#do_action*/}
                </div>
        
        </>        
    )
  }
  export default Cart;
  