import React, {Component, useEffect, useState} from 'react';
import axios from "axios";
import {useParams,useLocation,useNavigate} from "react-router-dom";
import FormError from '../Error/FormError';
// import api from '../api';

function Listcomment(props) {

    const [data, setData] = useState('');

    function fetchData(){
        let data = props.getCmt;
        if(data.length > 0){
            return data.map((value,key)=>{
                if(value.id_comment == 0){
                    return (
                        <>
                        <li  className="media" key={key}>
                            <a className="pull-left" href="#">
                            <img className="media-object" style={{width: "100px", height:"100px"}} src={"http://localhost:8087/laravel8/public/upload/user/avatar/" +value.image_user} alt="" />
                            </a>
                            
                            <div  className="media-body">
                            <ul className="sinlge-post-meta">
                                {/* <li><i className="fa fa-user" />{value.id}</li> */}
                                <li><i className="fa fa-user" />{value.name_user}</li>
                                {/* <li><i className="fa fa-clock-o" /> 1:33 pm</li> */}
                                <li><i className="fa fa-calendar" />{value.created_at}</li>
                            </ul>
                            <p>{value.comment}</p>
                            <a id={value.id} onClick = {props.replycmt} className="btn btn-primary" ><i className="fa fa-reply" />Replay</a>
                            </div>
                        </li>
                        { 
                            data.map((value2,key2)=>{
                                if(value2.id_comment == value.id){
                                    return (
                                        <li className="media second-media">
                                            <a className="pull-left" href="#">
                                                <img className="media-object" style={{width: "100px", height:"100px"}} src={"http://localhost:8087/laravel8/public/upload/user/avatar/" +value2.image_user} alt="" />
                                            </a>
                                            <div className="media-body">
                                                <ul className="sinlge-post-meta">
                                                <li><i className="fa fa-user" />{value2.name_user}</li>
                                                {/* <li><i className="fa fa-clock-o" /> 1:33 pm</li> */}
                                                <li><i className="fa fa-calendar" />{value2.created_at}</li>
                                                </ul>
                                                <p>{value2.comment}</p>
                                                <a id={value2.id} onClick = {props.replycmt} className="btn btn-primary" href><i className="fa fa-reply" />Replay</a>
                                            </div>
                                        </li>
                                    )
                                }
                            })
                        }
                        </>
                    );
                }
            })
        }
               
        }
    return (
        <div className="response-area">
                    <h2>RESPONSES</h2>
                    <ul className="media-list">
                        {fetchData()}
                    </ul>					
        </div>
           
       
    );

  }
  
  export default Listcomment;