import React, {Component, useEffect, useState} from 'react';
import axios from "axios";
import {useParams,useLocation,useNavigate} from "react-router-dom";
import FormError from '../Error/FormError';
import Detail from './Detail';
import Listcomment from './Listcomment';
// import api from '../api';

function Comment(props) {

        const idrl = props.idrl;
        // console.log(props.idrl);
        const id = props.id;
        const [inputs, setInputs] = useState({
            message:"",
        })

        const [errors, setErrors] = useState({});

        const handleInput = (e) => {
            const nameInput = e.target.name;
            const value = e.target.value;
            setInputs(state => ({...state,[nameInput]:value}))
        }

        
        function handleSubmit(e){
            
            e.preventDefault();
            let errorsSubmit={};

           
            if(inputs.message ==''){
                errorsSubmit.message = "Vui lòng nhập bình luận";
                setErrors(errorsSubmit);
            }
            else{

                const data =  localStorage.getItem("appState");
                const userData = JSON.parse(data);
    
                let url = 'http://localhost:8087/laravel8/public/api/blog/comment/' + id;
                let accessToken = userData.token;
    
                let config = { 
                    headers: { 
                    'Authorization': 'Bearer '+ accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                    } 
                };
            
                
                const formData = new FormData();
                formData.append('id_blog', id );
                formData.append('id_user', userData.Auth.id);
                formData.append('id_comment', idrl ? idrl : 0);
                formData.append('comment', inputs.message);
                formData.append('image_user', userData.Auth.avatar);
                formData.append('name_user', userData.Auth.name);
            

                axios.post( url, formData, config)
                .then(res =>{
                    console.log(res.data.data)
                    props.getNComment(res.data.data)
                    console.log('Thành công')
                })
                .catch(error => console.log(error))  

               
            }

        }
        
    
    return (
      
            <div className="replay-box">
                <div className="row">
                <div className="col-sm-12">
                <FormError  errors={errors}/>

                    <h2>Leave a replay</h2>
                    <div className="text-area">
                    <div className="blank-arrow">
                        <label>Your Name</label>
                    </div>
                    <span>*</span>
                    <form onSubmit={handleSubmit}>
                        <textarea name="message" rows={11} onChange={handleInput} />
                        <button type="submit" className="btn btn-primary" href>post comment</button>
                    </form>
                    </div>
                </div>
                </div>
            </div>
            
    );
  }
  
  export default Comment;