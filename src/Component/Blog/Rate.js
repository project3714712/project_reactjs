import React, { useState, useEffect} from 'react'
import { Rating } from 'react-simple-star-rating'
import StarRatings from 'react-star-ratings';
import axios from "axios";

export function Rate(props) {
    const [rating, setRating] = useState(0)
    const [data, setData] = useState('');
    let sum = 0
    useEffect(() => {
        axios.get('http://localhost:8087/laravel8/public/api/blog/rate/' +props.id) 
        .then(response => {
            setData(response.data.data)
            // console.log(response.data.data)
                for(var i=0; i<response.data.data.length; i++){

                    sum = sum + response.data.data[i]['rate']
                }
            sum = sum / response.data.data.length
            setRating(sum)
        })
        .catch(function (error){
            console.log(error)
        })
    },[])
    console.log(rating)
    function changeRating( newRating, name ) {
        setRating(newRating)
        
        const user =  localStorage.getItem("appState");
        const userData = JSON.parse(user);

        let accessToken = userData.token;
        
        let config = { 
            headers: { 
            'Authorization': 'Bearer '+ accessToken,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
            } 
        };

        const data = {
            blog_id: props.id,
            user_id: userData.Auth.id,
            rate: newRating
        }
            
        axios.post("http://localhost:8087/laravel8/public/api/blog/rate/" +props.id, data, config)
                .then(res=>{
                    if(res.data.errors){
                        console.log(res.data.errors);
                    }
                    else{
                        console.log('Thành công')
                    }
                })
            .catch(error => console.log(error))
        
    }
    
    
return (
    <StarRatings
      rating={rating}
      starRatedColor="yellow"
      changeRating={changeRating}
      numberOfStars={5}
      name='rating'
    />
  );

}
export default Rate;


  
