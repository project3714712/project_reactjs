import React, {Component, useEffect, useState} from 'react';
import axios from "axios";
import {useParams,useLocation,useNavigate} from "react-router-dom";
import FormError from '../Error/FormError';
import Comment from './Comment';
import Listcomment from './Listcomment';
import Rate from './Rate';
// import api from '../api';

function Detail(props) {

    let params = useParams();
    let id = params.id;
    const [data, setData] = useState('');
    const [getCmt, setCmt] = useState('');
    const [getidcmt, setidcmt] = useState('');
    

        useEffect(() => {
            axios.get('http://localhost:8087/laravel8/public/api/blog/detail/' +params.id) 
            .then(response => {
                setData(response.data.data)
                setCmt(response.data.data.comment)
             
            })
            .catch(function (error){
                console.log(error)
            })
        },[])

        function getNComment(data){
            setCmt(getCmt.concat(data))
        }

        
        const replycmt = (e) => {
            e.preventDefault();
            setidcmt(e.target.id)
        };

        function fetchData(){
            if(Object.keys(data).length > 0){
                return (
                        <div className="product-details">
                        <div className="col-sm-5">
                            <div className="view-product">
                            <img src="images/product-details/1.jpg" alt="" />
                            <a href="images/product-details/1.jpg" rel="prettyPhoto"><h3>ZOOM</h3></a>
                            </div>
                            <div id="similar-product" className="carousel slide" data-ride="carousel">
                            {/* Wrapper for slides */}
                            <div className="carousel-inner">
                                <div className="item active">
                                <a href><img src="images/product-details/similar1.jpg" alt="" /></a>
                                <a href><img src="images/product-details/similar2.jpg" alt="" /></a>
                                <a href><img src="images/product-details/similar3.jpg" alt="" /></a>
                                </div>
                                <div className="item">
                                <a href><img src="images/product-details/similar1.jpg" alt="" /></a>
                                <a href><img src="images/product-details/similar2.jpg" alt="" /></a>
                                <a href><img src="images/product-details/similar3.jpg" alt="" /></a>
                                </div>
                                <div className="item">
                                <a href><img src="images/product-details/similar1.jpg" alt="" /></a>
                                <a href><img src="images/product-details/similar2.jpg" alt="" /></a>
                                <a href><img src="images/product-details/similar3.jpg" alt="" /></a>
                                </div>
                            </div>
                            {/* Controls */}
                            <a className="left item-control" href="#similar-product" data-slide="prev">
                                <i className="fa fa-angle-left" />
                            </a>
                            <a className="right item-control" href="#similar-product" data-slide="next">
                                <i className="fa fa-angle-right" />
                            </a>
                            </div>
                        </div>
                        <div className="col-sm-7">
                            <div className="product-information">{/*/product-information*/}
                            <img src="images/product-details/new.jpg" className="newarrival" alt="" />
                            <h2>{data.title}</h2>
                            <p>Web ID: {data.id}</p>
                            <img src="images/product-details/rating.png" alt="" />
                            <span>
                                <span>US $59</span>
                                <label>Quantity:</label>
                                <input type="text" defaultValue={3} />
                                <button type="button" className="btn btn-fefault cart">
                                <i className="fa fa-shopping-cart" />
                                Add to cart
                                </button>
                            </span>
                            <p><b>Availability:</b> In Stock</p>
                            <p><b>Condition:</b> New</p>
                            <p><b>Brand:</b> E-SHOPPER</p>
                            <a href><img src="images/product-details/share.png" className="share img-responsive" alt="" /></a>
                            </div>{/*/product-information*/}
                        </div>
                        </div>
                    )
            }
        }

    return (
        <div className="col-sm-9 padding-right">

            {fetchData()}

            <div className="category-tab shop-details-tab">
            <div className="col-sm-12">
                <ul className="nav nav-tabs">
                <li><a href="#details" data-toggle="tab">Details</a></li>
                <li><a href="#companyprofile" data-toggle="tab">Company Profile</a></li>
                <li><a href="#tag" data-toggle="tab">Tag</a></li>
                <li className="active"><a href="#reviews" data-toggle="tab">Reviews (5)</a></li>
                </ul>
            </div>
            <div className="tab-content">
                <div className="tab-pane fade" id="details">
                <div className="col-sm-3">
                    <div className="product-image-wrapper">
                    <div className="single-products">
                        <div className="productinfo text-center">
                        <img src="images/home/gallery1.jpg" alt="" />
                        <h2>$56</h2>
                        <p>Easy Polo Black Edition</p>
                        <button type="button" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</button>
                        </div>
                    </div>
                    </div>
                </div>
                </div>

                
                <div className="tab-pane fade active in" id="reviews">
                <div className="col-sm-12">
                    <ul>
                    <li><a href><i className="fa fa-user" />EUGEN</a></li>
                    <li><a href><i className="fa fa-clock-o" />12:41 PM</a></li>
                    <li><a href><i className="fa fa-calendar-o" />31 DEC 2014</a></li>
                    </ul>
                    <p>{data.description}</p>
                    <p><b>Write Your Review</b></p>
                    <form action="#">
                    <span>
                        <input type="text" placeholder="Your Name" />
                        <input type="email" placeholder="Email Address" />
                    </span>
                    <textarea name defaultValue={""} />
                    <b>Rating: </b> <img src="images/product-details/rating.png" alt="" />
                    <button type="button" className="btn btn-default pull-right">
                        Submit
                    </button>
                    </form>
                </div>
                </div>
            </div>
            </div>

            <Rate id={id} />

            <Listcomment getCmt={getCmt} replycmt={replycmt}/>                  
            <Comment id = {id} getNComment = {getNComment} idrl={getidcmt} />
      </div>        
    );
  }
  
  export default Detail;