import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost:8087/laravel8/public/api/',
    timeout: 1000,
  });

  export default api;