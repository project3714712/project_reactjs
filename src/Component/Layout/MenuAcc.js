import React from 'react';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    useNavigate
} from 'react-router-dom';
function MenuAcc(props) {
        return(
        <div>
            <div className="col-sm-4">
                <div className="mainmenu pull-left">
                    <ul className="nav navbar-nav collapse navbar-collapse">
                    <li><Link to="/account/update">Account</Link></li>
                    <li><Link to="/account/product/list">My Products</Link></li>
                    </ul>
                </div>	
            </div>
        </div>
        )
}

export default MenuAcc;